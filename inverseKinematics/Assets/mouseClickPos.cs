﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseClickPos : MonoBehaviour {

    public Transform motherAxis, daughterAxis, grandDaughterAxis, endEffector;

    public Transform showMotherAxis, showDaughterAxis, showGrandDaughterAxis;



    Quaternion origRot1, origRot2, origRot3;
    Quaternion newRot1, newRot2, newRot3;

    bool isSlerping = false;


    private float timeCount = 0.0f;

    void Update()
    {


        if (isSlerping)
        {
            Debug.Log("slerping");

            motherAxis.rotation = Quaternion.Slerp(origRot1, newRot1, timeCount);
            daughterAxis.rotation = Quaternion.Slerp(origRot2, newRot2, timeCount);
            grandDaughterAxis.rotation = Quaternion.Slerp(origRot3, newRot3, timeCount);

            showMotherAxis.rotation = Quaternion.Slerp(origRot1, newRot1, timeCount);
            showDaughterAxis.rotation = Quaternion.Slerp(origRot2, newRot2, timeCount);
            showGrandDaughterAxis.rotation = Quaternion.Slerp(origRot3, newRot3, timeCount);


            timeCount = timeCount + Time.deltaTime;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            isSlerping = false;
            timeCount = 0.0f;

            Vector3 v3 = Input.mousePosition;
            v3.z = 10.0f;
            v3 = Camera.main.ScreenToWorldPoint(v3);

            Debug.Log("mouse x: " + v3.x + "mouse y: " + v3.y + "mouse z: " + v3.z);

            origRot1 = motherAxis.rotation;
            origRot2 = daughterAxis.rotation;
            origRot3 = grandDaughterAxis.rotation;

            pointArmToPosition(v3);

        }
    }

    void pointArmToPosition(Vector3 pos)
    {
        //motherAxis.LookAt(pos);
        //daughterAxis.LookAt(pos);

        isSlerping = false;

        int i = 0;
        int j = 0;

        float closestDist = Vector2.Distance(pos, endEffector.position);

 
        int bestAngleMother = 0;
        int bestAngleDaughter = 0;

        while (i < 360)
        {
            motherAxis.localEulerAngles = new Vector3(i, -90f, 0f);

            //Debug.Log("i = " + i);
            while (j < 360)
            {
                daughterAxis.localEulerAngles = new Vector3(j, 0f, 0f);

                grandDaughterAxis.LookAt(pos);

                float currentDist = Vector2.Distance(pos, endEffector.position);

                if (currentDist < closestDist)
                {
                    closestDist = currentDist;
                    bestAngleMother = i;
                    bestAngleDaughter = j;
                    //Debug.Log("closest distance: " + closestDist + " at " + i + "° mother " + j + "° daughter");
                }


                j += 10;
            }
            j = 0;

            i += 10;
            
        }

        Debug.Log("closest distance: " + closestDist + " at " + bestAngleMother + "° mother " + bestAngleDaughter + "° daughter");

        motherAxis.localEulerAngles = new Vector3(bestAngleMother, -90f, 0f);
        daughterAxis.localEulerAngles = new Vector3(bestAngleDaughter, 0f, 0f);
        grandDaughterAxis.LookAt(pos);

        newRot1 = motherAxis.rotation;
        newRot2 = daughterAxis.rotation;
        newRot3 = grandDaughterAxis.rotation;


        isSlerping = true;
        //moveArmToPosition(rot1, rot2, rot3, origRot1, origRot2, origRot3);        

    }

   /* void moveArmToPosition(Quaternion newRot1, Quaternion newRot2, Quaternion newRot3, Quaternion oldRot1, Quaternion oldRot2, Quaternion oldRot3)
    {
        
    }*/
}

