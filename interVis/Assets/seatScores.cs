﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class seatScores : MonoBehaviour
{

    //public Text debugtext;

    // Use this for initialization

    int[][] scores;

    public GameObject airFlowArrows;
    public GameObject peopleCircLines;

    public GameObject wheelChairAccLines;
    public GameObject exits;
    public GameObject temperatureArrows;
    public GameObject powerOutlets;
    public GameObject wifiRouter;
    public GameObject specSeats;

    public GameObject canvasAirFlows;
    public GameObject canvasPeopleCirc;
    public GameObject canvasOccHistory;
    public GameObject canvasWheelChair;
    public GameObject canvasExits;
    public GameObject canvasTemperature;
    public GameObject canvasPowerOutlets;
    public GameObject canvasVisibility;
    public GameObject canvasWifi;
    public GameObject canvasSpecSeats;



    void Start()
    {

        scores = new int[10][];


        //Debug.Log("começou");

        //DATA SET 1:

           int[] airflow = {10,  9,  8,  7,  6,  5,  5,  4,  3,  2,  1,
   10,  9,  8,  7,  6,          4,  3,  2,  1,
    9,  9,  8,  7,  6,          4,  3,  2,  1,
    8,  8,  7,  6,  5,          3,  2,  1,  0};

           int[] peopleCirculating = {10,  9,  8,  7,  5,  5,  4,  5,  8,  9, 10,
   10,  9,  8,  7,  5,          5,  8,  9, 10,
   10,  9,  8,  7,  6,          5,  8,  9, 10,
   10, 10, 10,  9,  9,          6,  8,  9, 10};

           int[] occupHist = {1, 10,  5,  6,  2,  3,  1, 10,  1,  4,  9,
    6,  5,  9, 10,  5,          9, 10,  3,  3,
    7,  5,  4,  7,  9,          5,  9,  7,  7,
    1,  1,  2,  2,  7,          7,  8,  9,  1};

           int [] wheelChairAccess = {0,  1,  2,  3,  8,  9, 10,  8,  2,  1,  0,
    0,  1,  2,  3,  8,          8,  3,  2,  0,
    0,  1,  2,  3,  8,          8,  3,  2,  0,
    9,  9,  9,  9,  9,         10, 10, 10, 10};

           int[] exitAccess = {0,  1,  2,  4,  6,  7,  7,  6,  5,  3,  0,
3,  4,  5,  6,  7,          7,  6,  5,  3,
5,  6,  6,  7,  8,          9,  8,  7,  5,
7,  7,  8,  9,  9,         10,  9,  9, 10};


           int[] nota_temperatura = {9,  8,  8,  8, 10, 10,  9,  8,  8,  8,  9,
    5,  5,  5,  6,  7,          8,  7,  7,  8,
    4,  3,  3,  4,  5,          8,  7,  7,  8,
    5,  4,  3,  4,  6,          9,  7,  7,  8};

           int[] powerOutlets = {10,  8,  5,  3,  3,  3,  3,  3,  5,  8, 10,
    8,  8,  5,  3,  3,          3,  5,  8,  8,
   10,  8,  5,  3,  3,          3,  5,  8, 10,
    8,  8,  5,  3,  3,          3,  5,  8,  8};

           int[] visibility =
           {
               2,  4,  5,  6,  7,  8,  8,  7,  5,  4,  2,
    6,  7,  7,  8,  9,          9,  8,  6,  5,
    7,  8,  8,  9, 10,         10,  9,  8,  7,
    8,  9, 10, 10, 10,         10,  9,  8,  6
           };

           int[] wifiSignal =
           {
               7,  7,  7,  8,  8,  8,  8,  8,  8,  8,  8,
    7,  8,  8,  8,  8,          8,  8,  8,  8,
    8,  8,  8,  8,  8,          9,  9,  9,  9,
    9,  9,  9,  9, 10,         10, 10, 10, 10
           };

           int[] specialSeats =
           {0, 10, 0, 0, 0, 0, 0, 0, 10, 10, 0,
                    0, 0, 10, 0, 0,       0, 10, 0, 0,
                    0, 0, 10, 0, 10,       0, 0, 0, 0,
                    10, 10, 10, 0, 10,       10, 10, 10, 10
           };

       
        //DATA SET 2:
/*
        int[] airflow = {0,  1,  2,  4,  5,  5,  6,  7,  8,  8,  8,
 1,  2,  3,  4,  6,          7,  8,  9,  9,
 1,  2,  3,  4,  6,          8,  8,  9,  9,
 2,  2,  3,  3,  5,          8,  9, 10, 10
        };

        int[] peopleCirculating = {8,  9,  8,  7,  5,  5,  4,  5,  8,  9,  8,
 8,  9,  8,  7,  5,          5,  8,  9,  8,
 8,  9,  8,  7,  8,          8,  8,  9,  8,
 9,  9,  9, 10, 10,         10, 10, 10,  9
        };

        int[] occupHist = {10,  9,  4,  9,  3,  3,  4,  3,  7,  5,  5,
 6,  7,  1,  8,  7,          7,  5, 10,  2,
 5,  9,  1, 10,  7,          1,  2,  9,  1,
 2,  1,  9, 10,  1,          9,  7,  5,  6
        };

        int[] wheelChairAccess = { 0,  1,  2,  3,  8, 10,  9,  8,  2,  1,  0,
 0,  1,  2,  3,  8,          8,  3,  2,  0,
 0,  1,  2,  3,  8,          8,  3,  2,  0,
10, 10, 10, 10, 10,          9,  9,  9,  9
        };

        int[] exitAccess = {0,  1,  2,  4,  6,  7,  7,  6,  5,  3,  0,
 3,  4,  5,  6,  7,          7,  6,  5,  3,
 5,  6,  6,  7,  8,          9,  8,  7,  5,
 7,  7,  8,  9,  9,          9, 10, 10,  9
        };


        int[] nota_temperatura = {10,  9,  9,  8,  9,  9,  9,  8,  8,  8,  9,
 9,  8,  7,  8,  7,          6,  5,  5,  5,
 8,  7,  7,  8,  5,          4,  3,  3,  4,
 8,  7,  7,  9,  6,          4,  3,  4,  5
        };

        int[] powerOutlets = {8,  8,  5,  3,  8, 10, 10,  8,  5,  8,  8,
 8,  8,  5,  3,  3,          3,  5,  8,  8,
 8,  8,  5,  3,  3,          3,  5,  8,  8,
10,  8,  5,  3,  3,          3,  5,  8,  10
        };

        int[] visibility =
        {8,  7,  6,  6,  4,  8, 10,  9,  9,  7, 10,
 8, 10,  8,  9,  6,         10,  8,  4,  5,
10,  9,  8,  7,  7,          9,  6,  8,  2,
 2,  7, 10,  8,  9,          5,  8,  5,  7
        };

        int[] wifiSignal =
        {10, 10, 10, 10,  9,  9,  9,  9,  9,  9,  9,
10,  9,  9,  9,  8,          8,  8,  8,  8,
 8,  8,  8,  8,  8,          8,  8,  8,  7,
 8,  8,  8,  8,  8,          8,  7,  7,  7
        };

        int[] specialSeats =
        {10,  0,  0,  0,  0, 10,  0,  0,  0, 10,  0,
 0, 10,  0,  0,  0,          0, 10, 10,  0,
10,  0, 10,  0, 10,         10,  0, 10, 10,
 0,  0, 10,  0, 10,         10,  0,  0,  0
        };
        */

        scores[0] = airflow;
        scores[1] = peopleCirculating;
        scores[2] = occupHist;
        scores[3] = wheelChairAccess;
        scores[4] = exitAccess;
        scores[5] = nota_temperatura;
        scores[6] = powerOutlets;
        scores[7] = visibility;
        scores[8] = wifiSignal;
        scores[9] = specialSeats;


        //debugtext.text = "setou notas";s

        int i = 0;
        for (i = 0; i < 38; i++)
        {
            for (int j = 0; j < 10; j++)
                scores[j][i] *= 10;
        }

        // debugtext.text = "mult 10";

        i = 0;

        changeAttribute(0);


    }

    public void changeAttribute(int attId)
    {
        int i = 0;

        airFlowArrows.SetActive(false);
        peopleCircLines.SetActive(false);

        wheelChairAccLines.SetActive(false);
        exits.SetActive(false);
        temperatureArrows.SetActive(false);
        powerOutlets.SetActive(false);
        wifiRouter.SetActive(false);

        specSeats.SetActive(false);

        canvasAirFlows.SetActive(false);
        canvasPeopleCirc.SetActive(false);
        canvasOccHistory.SetActive(false);
        canvasWheelChair.SetActive(false);
        canvasExits.SetActive(false);
        canvasTemperature.SetActive(false);
        canvasPowerOutlets.SetActive(false);
        canvasVisibility.SetActive(false);
        canvasWifi.SetActive(false);
        canvasSpecSeats.SetActive(false);

        foreach (Transform child in transform)
        {

            //  Debug.Log("child #" + i);

            child.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = scores[attId][i].ToString();

            child.GetChild(0).GetComponent<materialColor>().changeColor();

            child.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text += "%";

            i++;
        }

        switch (attId)
        {
            case 0:     //air flow
                airFlowArrows.SetActive(true);
                canvasAirFlows.SetActive(true);
                break;
            case 1:     //people circulation
                peopleCircLines.SetActive(true);
                canvasPeopleCirc.SetActive(true);
                break;
            case 2:     //occupation history
                canvasOccHistory.SetActive(true);
                break;                      //no special vis
            case 3:     //wheelchair accessibility
                wheelChairAccLines.SetActive(true);
                canvasWheelChair.SetActive(true);
                break;
            case 4:     //exit acess
                exits.SetActive(true);
                canvasExits.SetActive(true);
                break;
            case 5:     //temperature
                temperatureArrows.SetActive(true);
                canvasTemperature.SetActive(true);
                break;
            case 6:     //power outlets
                powerOutlets.SetActive(true);
                canvasPowerOutlets.SetActive(true);
                break;
            case 7:     //visibility
                canvasVisibility.SetActive(true);
                break;                  //no special vis
            case 8:     //wifi signal
                wifiRouter.SetActive(true);
                canvasWifi.SetActive(true);
                break;
            case 9:     //special seats
                specSeats.SetActive(true);
                canvasSpecSeats.SetActive(true);
                break;
            default:
                break;

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
