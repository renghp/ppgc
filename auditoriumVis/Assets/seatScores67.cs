﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class seatScores67 : MonoBehaviour
{

    //public Text debugtext;

    // Use this for initialization

    int[][] scores;

    bool chairStatus = true;

    bool plaqueStatus = false;

    int lastId = 0;

   // Component cp; //= grandchild.GetChild(1).GetChild(2).gameObject.GetComponent<changeScoreTex>();

    // public GameObject clonablePlane;

    //public GameObject clonableSeatLabel;

    /* public GameObject airFlowArrows;
     public GameObject peopleCircLines;

     public GameObject wheelChairAccLines;
     public GameObject exits;
     public GameObject temperatureArrows;
     public GameObject powerOutlets;
     public GameObject wifiRouter;
     public GameObject specSeats;

     public GameObject canvasAirFlows;
     public GameObject canvasPeopleCirc;
     public GameObject canvasOccHistory;
     public GameObject canvasWheelChair;
     public GameObject canvasExits;
     public GameObject canvasTemperature;
     public GameObject canvasPowerOutlets;
     public GameObject canvasVisibility;
     public GameObject canvasWifi;
     public GameObject canvasSpecSeats;*/



    void Start()
    {

        scores = new int[10][];


        //Debug.Log("começou");

        //DATA SET 1:


        int[] airflow =
           {100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,

            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
            100,  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0,
                  91,  82,  73,  64,  55,  56,  47,  38,  29,  19,  9,  0};

        int[] peopleCirculating =
           {74,  57,  66,  86,   3,  37,  44,  53,  67,   8,  13,  93,  50,
            36,  14,  45,  42,  64,  83,  41,  29,  12,  77,  40,  82,  38,
            96,  18,  78,  11,  33,  31,  65,  90,  71,   1,   2,  60,  94,
            54,  56,   6,  49,  26,  28,   9,  99,  72,  88,  58, 100,  55,
            19,  21,  76,  47,  24,  70,  22,  32,  48,   4,  69,  59,  84,

            87,  77,  95,  73,  15,  86,  51,   2,  84,  70,  46,  41,  56,
            7,   55,  83,  25,  78,  29,  16,  82,  26,  63,  57,  76,  18,
            27,  44,  49,  98,  12,  10,  42,   1,  21,   9,  23,  37, 100,
            33,   3,  40,  35,  94,  48,  45,  67,  69,  20,  80,  89,  50,
            75,  24,  30,  88,  39,  22,  71,  60,  47,  61,  62,  43,  17,
                 68,  19,  81,  72,  32,   6,  91,  66,  58,  59,  52,  53};

        int[] occupHist =
           {44,  92,  86,  33,  25,   8,  45,  66,  57,  72,  67,  50,  82,
            57,  74,  8,   2,   72,  86,   5,  17,  40,  75,  94,  56,  30,
            74,  24,  4,   19,  81,  96,  58,  59,  33,  40,  76,  10,  67,
            25,  30,  39,  55,  21,  62,   4,  29,  49,  48,  88,   2,  64,
            65,  17,  79,  18,  5,   32,  95,  40,  49,  69,  19,  60,  29,

            29,  38,  45,  91,  74,  67,  54,  84,  71,  86,  40,  21,  94,
            87,  42,  78,  23,  73,  96,  50,  83,  65,  54,  85,  79,   1,
            50,  21,  69,  61,  37,  58,  35,  26,  39,   2,  63,  38,   7,
            45,  47,  38,  26, 100,  95,  83,  28,  21,  67,  55,  79,  94,
            61,  40,  79,  18,  64,  34,  26,  36,   4,   1,  74,  89,  63,
                  8,  19,  30,   3,  93,  52,  98,  46,   1,  63,  61,  99};

        int[] wheelChairAccess =
           {84,   8,  65,  92,  13,  46,  24,  44,  22,  51,  50,  28,  68,
            57,  49,  32,  63,  85,  17,  89,  77,  68,  83,  53,  72,  24,
            33,  22,  36,  25,  70,  99,  78,  12,  17,  45,  92,  16,  23,
            11,  97,   6,  37,  32,  24,  28,  23,  16,  20,  12,  19,  51,
            68,  62,  25,  10,  79,  66,  47,  87,  35,  52,   2,  80,  49,

            26,  27,  44,  97,  62,  16,  85,   1,   2,  93,  8,   37,  82,
            93,  30,  80,  19,  82,  75,  79,  62,  12,  63,  14,  74,  99,
            44,  87,  32,  16,  51,   5,  39,  66,   4,  95,  19,  54,  24,
             3,  82,   9,  93,  7,   24,  58,  15,  14,  61,  47,  55,  49,
            52,  63,  26, 100,  68,  13,  27,  78,  60,  32,   9,  97,  39,
                 90,  51,  80,  40,  70,  96,  41,  72,  67,  46,   7,  48};

        int[] exitAccess =
           {37,  72,  28,  41,  90,  58,  43,  95,  45,  25,  59,  74,  68,
            47,   4,  84,  92,  28,  19,  36,  71,  56,  85,  77,  79,   9,
            89,   2,   7,  41,  25,  24,  27,  92,  68,  43,  83,  33,  76,
            69,  58,  13,  99,  75,  44,  19,  64,  66,  49,  33,  51,  95,
            58,  26,  32,  45,  41,  94,  79,  64,   8,  83,   3,  50,  27,

            49,  26,   1,  60,  86,  12,  47,  75,  18,  51,  78,  62,  41,
            11,  75,  55,  39,  86,  40,  33,  30,  64,  29,  27,   6,  16,
            43,  86,  93,  68,  42,   4,  97,  31, 100,  54,  20,  63,  72,
            43,  99,  58,  39,  34,  21,  10,  71,  61,  14,  85,  74,  20,
            95,  93,  20,   3,   4,  29,  76,  18,  82,  50,  43,  54,  10,
                 62,  39,  51,  60,  91,  90,  70,   5,  27,   4,  20,  59};


        int[] nota_temperatura =
           {43,  34,  64,  8,   44,  50,  89,  99,  85,  25,  63,  35,  18,
            29,  96,  10,  99,  31,  43,  66,  53,  35,  52,  54,  89,   9,
            35,  85,  47,  58,  34,   2,  97,  14,  82,  18,  66, 100,  3,
            56,  10,  65,  62,  76,  73,  74,  98,  28,  27,  66,  95,  58,
            64,  60,  62,  16,  45,  81,  59,  82,  48,  49,  44,  33,  51,

            85,  86,  80,   8,  99,  67,  65,  49,  27,  46,  96,  14,  47,
            93,  53,   5,  46, 100,  29,  11,  65,  77,  88,  78,  27,  79,
            14,  23,  94,  35,  22,  52,  99,  10,  53,  76,  79,  29,  67,
            98,   8,  46,  14,  68,  58,   1,  55,  82,  48,  97,  74,  65,
            45, 100,  60,  51,  96,   6,   5,  88,  30,  87,  40,   4,  26,
                 82,  46,  17,  59,  39,  99,  27,  85,  42,  58, 100,  94};

        int[] powerOutlets =
           {35,  51,  63,  91,   8,  33,  89,   6,   9,  80,  76,  13,  56,
            16,  96,   2,  83,   7,  53,  88,  72,  89,  87,  79,  39,   9,
            59,  95,  39,  31,  61,  12,  55,  17,  45,  98,  46,  57,  47,
            22,  99,  42,  81,  12,  15,  78,   3,  49,  76,  43,  46,  55,
            12,  83,  38,   3,  68,  51,  31,  28,   9,  40,   8,  67,  49,

            21,  22,   2,  73,  71,  50,  30,  70,  65,  13,  90,  29,   6,
            84,  49,   2,   9,  75,  45,  80,  70, 100,  59,  81,  44,  35,
            22,  76,  82,   5,  57,  95,  56,  47,  68,  88,  83,  77,  71,
            84,  52,   8,  15,  95,  31,  91,  44,  63,  36,   3,  37,  12,
            7,   39,  29,  33,  23,  76,  85,  40,  98,  67,  77,  10,  48,
                 34,  50,  22,  46,  54,  79,  77,  39,  14, 100,  27,  30};

        int[] visibility =
           { 9,  44,  51,  69,  16,   8,  23,  82,  98,  55,  36,  68,  20,
             8,  75,  72,  86,  13,  78,  24,  32,  59,  44,  88,  54,  12,
             5,  16,  80,  52,  84,  57,  25,  39,  29,  56,  88,  43,  77,
            21,  53,  62,  40,   2,  49,  27,  64,  30,  50,  28,  52,  76,
             2,  25,   7,  50,  29,  40,  61,  48,   6,  41,  62,  37,  96,

            25,  79,  34,   3,  87,  32,  86,  61,  77,   6,  60,  43,  26,
            84,  49,  30,  66,  62,  82,  22,  54,  23,  55,  39,  42,  25,
            72,  83,  76,  23,  82,  49,  61,  99,  51,  35,   3,  40,  48,
            14,  44,  65,   1,   3,  32,  51,  38,  52,  48,  10,   4,   2,
            43,  57,  70,  87,  77,  26,   1,  85,  82,  28,  53,  76,  93,
                 17,  10,  63,  68,  76,  65,  80,  66,  18,  26,  93,  16};

        int[] wifiSignal =
           {86,   1,  88,  15,  64,  52,  33,  70,  18,  87,  92,  44,  67,
            25,  19,  66,  30,  69,   9,  72,   3,  52,  21,  85,  11, 100,
            33,  34,  11,  99,  69,  89,  62,  76,  98,  17,   3,   2,  59,
            64,  91,  46,  35,  87,  53,  20,  95,   5,  69,  75,   3,  61,
            60,  38,  92,  56,  87,   3,  96,  45,  70,  93,   8,  22,  83,

            66,  49,  40,  46,  38,  16,  39,  42,  43,  69,  92,   9,  75,
             3,  64,  50,  96,  99,  12,  60, 100,  15,  18,  29,  61,  74,
            69,  95,  68,   2,  17,  44,   3,  24,  97,  90,  84,  22,  49,
             3, 100,  75,  31,  53,  66,  26,  20,  49,  59,  87,  65,  50,
            56,  73,   3,  40,  86,  45,  74,  35,  50,  16,  55,  58,  82,
                 33,   4,  11,  92,  69,  88,  97,  56,  51,  68,   2,  9};

        int[] specialSeats =
           {10,  78,  11,  84,  25,  30,  63,  72,  39,  34,  91,  76,  16,
            21,  26,  22,  87,  74,  20,  64,  31,  48,  75,  41,  54,  72,
            12,  27,  67,  41,  31,  11,  32,  38,  44,  56,  71,  37,  14,
            48,  47,  89,  59,  54,  93,  17,  11,  68,   3,  81,  95,  60,
            64,  33,  20,  46,  51,  75,  60,  69,  31,  53,   8,  58,  81,

            69,  48,  91,  21,  39,  30,  18,  53,  65,  10,   5,  44,  98,
            42,  90,  56,  59,  31,  44,  92,  73,  50,  64,   1,  52,   2,
            82,  30,  41,  91,  87,  59,  23,  47,  79,  50,   4,  25,  89,
            36,   3,   8,  71,   1,  22,  60,  47,  59,  24,   6,  30,   7,
            46,  65,  17,  54,  52,  91,  63,  78,  14,  75,  39,   4,  25,
                 42,  28,  79,  37,  34,  92,  71,  26,   3,  17,   5,  67};


        scores[0] = airflow;
        scores[1] = peopleCirculating;
        scores[2] = occupHist;
        scores[3] = wheelChairAccess;
        scores[4] = exitAccess;
        scores[5] = nota_temperatura;
        scores[6] = powerOutlets;
        scores[7] = visibility;
        scores[8] = wifiSignal;
        scores[9] = specialSeats;



        changeAttribute(lastId);


    }

    public void switchChairs(bool status)
    {
        chairStatus = status;

        changeAttribute(lastId);

    }

    public void switchPlaques(bool status)
    {
        plaqueStatus = status;

        changeAttribute(lastId);

    }

    public void changeAttribute(int attId)
    {
        lastId = attId;
        int i = 0;

        /*  airFlowArrows.SetActive(false);
          peopleCircLines.SetActive(false);

          wheelChairAccLines.SetActive(false);
          exits.SetActive(false);
          temperatureArrows.SetActive(false);
          powerOutlets.SetActive(false);
          wifiRouter.SetActive(false);

          specSeats.SetActive(false);

          canvasAirFlows.SetActive(false);
          canvasPeopleCirc.SetActive(false);
          canvasOccHistory.SetActive(false);
          canvasWheelChair.SetActive(false);
          canvasExits.SetActive(false);
          canvasTemperature.SetActive(false);
          canvasPowerOutlets.SetActive(false);
          canvasVisibility.SetActive(false);
          canvasWifi.SetActive(false);
          canvasSpecSeats.SetActive(false);*/

        foreach (Transform child in transform)
        {
            
            //  Debug.Log("child #" + i);

            //child.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = scores[attId][i].ToString();

            foreach (Transform grandchild in child)
            {

                //permanent:

                grandchild.GetChild(0).gameObject.SetActive(chairStatus);     //deactivates the chair

                grandchild.GetChild(1).GetChild(2).gameObject.SetActive(chairStatus); //deactivates the chair score texture

                //grandchild.GetChild(1).GetChild(2).localScale = new Vector3(0.02f, 1f, 0.02f);

                grandchild.GetChild(0).GetComponent<materialColor>().changeColor(scores[attId][i]);   //colors the chair




                grandchild.GetChild(1).GetChild(2).gameObject.AddComponent<changeScoreTex>();
                grandchild.GetChild(1).GetChild(2).gameObject.GetComponent<changeScoreTex>().changeScore(scores[attId][i]);

                grandchild.transform.GetChild(1).GetChild(0).gameObject.SetActive(plaqueStatus);    //activates the plaques

                grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<materialColor>().changeColor(scores[attId][i]);   //colors the plaques
                grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetChild(0).gameObject.AddComponent<changeScoreTex>();
                grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetChild(0).gameObject.GetComponent<changeScoreTex>().changeScore(scores[attId][i]);



                //deprecated:

                //grandchild.transform.GetChild(1).GetChild(0).GetChild(0).localPosition = new Vector3(0f, 0.8f, 0f);     //plaque line
                //grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).gameObject.SetActive(false);   //deactivates the plaque canvas

                //grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetComponent<materialColor>().changeColor(scores[attId][i]);       //colors the plaque

                //grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = scores[attId][i].ToString() + "%";     //numbers the plaque canvas


                //grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).gameObject.SetActive(false); //deactivates the numbering in the canvas






                // grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = false;      //deactivates the cube

                /*GameObject planeClone;
                planeClone = Instantiate(clonablePlane, clonablePlane.transform.position, clonablePlane.transform.rotation);

                planeClone.transform.parent = grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1);

                planeClone.transform.localPosition = clonablePlane.transform.localPosition;
                planeClone.transform.localRotation = clonablePlane.transform.localRotation;
                planeClone.transform.localScale = clonablePlane.transform.localScale;*/

                //planeClone.AddComponent<materialColor>();




                /* grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<MeshCollider>().enabled = false;

                 grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;

                 grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;

                 grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

                 grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().receiveShadows = false;

                 grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().motionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion;

                 grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().allowOcclusionWhenDynamic = false;*/

                /*grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetComponent<CanvasScaler>().enabled = false;  //deactivates the plaque canvas scaler
                grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetComponent<GraphicRaycaster>().enabled = false;
                grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().supportRichText = false;
                grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().raycastTarget = false;

                grandchild.transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetChild(1).gameObject.SetActive(false);   //deactivates the plaque background image*/

                //grandchild.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>().text = scores[attId][i].ToString() + "%";    //numbers the chair canvas

                //grandchild.transform.GetChild(1).GetChild(1).gameObject.SetActive(false);   //deactivates the chair canvas

                /*grandchild.GetChild(0).gameObject.SetActive(true);     //deactivates the chair

                grandchild.GetChild(0).gameObject.GetComponent<MeshRenderer>().reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;

                grandchild.GetChild(0).gameObject.GetComponent<MeshRenderer>().lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;

                grandchild.GetChild(0).gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
               
                grandchild.GetChild(0).gameObject.GetComponent<MeshRenderer>().receiveShadows = false;
                
                grandchild.GetChild(0).gameObject.GetComponent<MeshRenderer>().motionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion;
                
                grandchild.GetChild(0).gameObject.GetComponent<MeshRenderer>().allowOcclusionWhenDynamic = false;

                

               grandchild.transform.GetChild(1).GetChild(1).GetComponent<CanvasScaler>().enabled = false;  //deactivates the plaque canvas scaler
                grandchild.transform.GetChild(1).GetChild(1).GetComponent<GraphicRaycaster>().enabled = false;
                grandchild.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>().supportRichText = false;
                grandchild.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>().raycastTarget = false;*/





                i++;
            }

            //child.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text += "%";


        }


        switch (attId)
        {
            /* case 0:     //air flow
                         // airFlowArrows.SetActive(true);
                         //  canvasAirFlows.SetActive(true);
                 break;
             case 1:     //people circulation
                         //   peopleCircLines.SetActive(true);
                         //   canvasPeopleCirc.SetActive(true);
                 break;
               case 2:     //occupation history
                   canvasOccHistory.SetActive(true);
                   break;                      //no special vis
               case 3:     //wheelchair accessibility
                   wheelChairAccLines.SetActive(true);
                   canvasWheelChair.SetActive(true);
                   break;
               case 4:     //exit acess
                   exits.SetActive(true);
                   canvasExits.SetActive(true);
                   break;
               case 5:     //temperature
                   temperatureArrows.SetActive(true);
                   canvasTemperature.SetActive(true);
                   break;
               case 6:     //power outlets
                   powerOutlets.SetActive(true);
                   canvasPowerOutlets.SetActive(true);
                   break;
               case 7:     //visibility
                   canvasVisibility.SetActive(true);
                   break;                  //no special vis
               case 8:     //wifi signal
                   wifiRouter.SetActive(true);
                   canvasWifi.SetActive(true);
                   break;
               case 9:     //special seats
                   specSeats.SetActive(true);
                   canvasSpecSeats.SetActive(true);
                   break;
             default:
                 break;*/

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
