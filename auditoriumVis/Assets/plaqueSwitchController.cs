﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloLensHandTracking;

public class plaqueSwitchController : MonoBehaviour
{

    public Transform placeHolder;

    private bool status;

    public GameObject virtualSeats;

    // Use this for initialization
    void Start()
    {

        status = false;
        //OnSelect();

    }

    // Update is called once per frame
    void Update()
    {

        transform.position = placeHolder.position;
        transform.eulerAngles = placeHolder.eulerAngles;

    }

    void OnSelect()
    {


        status = !status;

        virtualSeats.GetComponent<seatScores67>().switchPlaques(status);

    }
}


