﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class followCurve : MonoBehaviour
{
    public Transform ghost2BFollowed;
    float time, time2;

    float ratio;

    float oldDist, newDist;

    //float highest, lowest;

    float dist;

    // Start is called before the first frame update
    void Start()
    {
        dist = 0.107f;
     /*   highest = 0.107f;
        lowest = 0.107f;*/

        ratio = 1f;
        time = 0f;
        time2 = 0f;

    }

    // Update is called once per frame
    void Update()
    {
       /* if (time >2)
        { 
            if (highest < dist)
            {
                highest = dist;
            }

            if (lowest > dist)
            {
                lowest = dist;
            }

        }*/
        oldDist = newDist;

       // Debug.Log("ratio" + (1/ratio).ToString());

       /* if (oldDist > 0.11)
        {
            time += ((1 / ratio) * 0.01f);
            time2 += ((1 / ratio) * 0.03f);
        }*/
        if (oldDist < 0.0977)
        {
            time += ((1 / ratio) * 0.019f);
            time2 += ((1 / ratio) * 0.057f);
        }
        else
        {
            time += ((1 / ratio) * 0.01f);
            time2 += ((1 / ratio) * 0.03f);
        }

        Vector3 oldPos = transform.position;

        Vector3 newPos = new Vector3 ((float)(15*Math.Cos(time)), (float)(3 * Math.Sin(time)), (float)((2 * Math.Sin(time2) + (1/5 * Math.Cos(time)))));

        transform.position = newPos;

        dist = Vector3.Distance(newPos, oldPos);

        newDist = dist;

        dist = (oldDist + newDist) / 2;

       /* Debug.Log("Highest dist:" + highest.ToString());
        Debug.Log("Lowest dist:" + lowest.ToString());*/

       // Debug.Log("Dist plane:" + dist.ToString() );

        //Debug.Log("time2:" + time2.ToString());

        ratio = dist / 0.107787625f;

        transform.LookAt(ghost2BFollowed);

       


    }
}
