﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class followCurveGhost : MonoBehaviour
{
    float time, time2;

    float ratio;

    float oldDist, newDist;

    float dist;

    // Start is called before the first frame update
    void Start()
    {

        ratio = 1f;
        time = 0f;
        time2 = 0f;
    }

    // Update is called once per frame
    void Update()
    {

        oldDist = newDist;



        if (oldDist < 0.0977)
        {
            time += ((1 / ratio) * 0.019f);
            time2 += ((1 / ratio) * 0.057f);
        }
        else
        {
            time += ((1 / ratio) * 0.01f);
            time2 += ((1 / ratio) * 0.03f);
        }


        Vector3 oldPos = transform.position;

        Vector3 newPos = new Vector3 ((float)(15*Math.Cos(time)), (float)(3 * Math.Sin(time)), (float)((2 * Math.Sin(time2) + (1/5 * Math.Cos(time)))));

        transform.position = newPos;

        dist = Vector3.Distance(newPos, oldPos);

        newDist = dist;

        dist = (oldDist + newDist) / 2;

        ratio = dist / 0.107787625f;




    }
}
